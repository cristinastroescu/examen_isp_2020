package examen.s2;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
public class Exercitiu2 implements ActionListener {
    JFrame frame = new JFrame("ISP");
    JButton button1 = new JButton("Click me!");
    JTextField textField1, textField2;
    JLabel label1, label2;

    public Exercitiu2() {
        makeGUI();
    }

    private void makeGUI() {
        frame.setLayout(null);
        frame.setLocation(200, 200);

        arrangeComponents();

        frame.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        frame.setSize(500, 300);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    private void arrangeComponents() {
        label1 = new JLabel("Introdu un text: ");
        label1.setBounds(50, 50, 125, 30);
        frame.add(label1);
        label1.setVisible(true);

        textField1 = new JTextField("");
        textField1.setBounds(240, 50, 230, 30);
        frame.add(textField1);
        textField1.setVisible(true);

        label2 = new JLabel("Numarul de caractere din text: ");
        label2.setBounds(50, 100, 175, 30);
        frame.add(label2);
        label2.setVisible(true);

        textField2 = new JTextField("");
        textField2.setBounds(240, 100, 230, 30);
        frame.add(textField2);
        textField2.setVisible(true);
        textField2.setEditable(false);

        button1.setBounds(150, 175, 200, 50);
        button1.setVisible(true);
        button1.addActionListener(this);
        frame.add(button1);

        SwingUtilities.getRootPane(button1).setDefaultButton(button1);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == button1)
        {
            String text = textField1.getText();
            int nr = text.length();

            textField2.setText(Integer.toString(nr));
        }
    }


}
